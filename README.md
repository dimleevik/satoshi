Requirements:

- Docker version 19.03.15
- docker-compose version 1.29.2

For building infrastructure (mysql+migration):
`docker-compose up -d`

Migration needs about 30 seconds to be completed.

If you want to check migrations, just use `docker ps -a | grep migration`. If container has status `Exited (0)`. You can run application.

Before you run application, you need to add host/port/credentials from smtp server to .development.env file.

After all, you need to run the app:
`npm ci; npm run dev`
