const express = require("express");
const routes = require("./routes");

const app = express();

app.use(express.json());
app.use(routes);

app.use(function (err, req, res, next) {
	console.log(err);
	res.status(500).send(err);
});

module.exports = app;
