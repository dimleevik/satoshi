const UserModel = require("../../models/users/userModel");
const smtpTransport = require("../../transports/smtp/smtpTransport");
const JwtService = require("../jwt/jwtService");

class AuthService {
	static async login(req, res) {
		const userData = req.body;

		const user = await UserModel.findOne({
			where: { email: userData.email, password: userData.password },
			raw: true,
		});
		if (user) {
			res.send({
				access_token: JwtService.sign({ ...user, password: "" }),
			});
			return;
		}
		res.status(400).send();
	}

	static async register(req, res) {
		const { email, password, role } = req.body;

		const isExist = await UserModel.findOne({
			where: { email },
			raw: true,
		});
		if (isExist) {
			res.status(400).send({ message: "User exists" });
			return;
		}
		await UserModel.create(
			{
				email,
				password,
				role,
			},
			{ raw: true }
		);
		res.send({ message: "registered" });
	}

	static async forgot(req, res, next) {
		const { email } = req.body;

		try {
			const user = await UserModel.findOne({
				where: { email },
				raw: true,
			});

			const response = await smtpTransport.sendMail({
				from: "dimleevik@gmail.com",
				to: email,
				subject: "Password recover",
				text: `Your password is ${user.password}`,
			});
			res.send({ accepted: response?.accepted });
		} catch (err) {
			next(err);
		}
	}
}

module.exports = AuthService;
