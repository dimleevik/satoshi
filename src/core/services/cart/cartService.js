const CartModel = require("../../models/cart/cartModel");

class CartService {
	static async create(req, res, next) {
		const { user, body } = req;
		try {
			const cartItem = await CartModel.create({
				productId: body.id,
				userId: user.id,
			});
			res.send({ message: cartItem });
		} catch (err) {
			next(err);
		}
	}

	static async list(req, res, next) {
		const { user } = req;
		try {
			const [cart] = await CartModel.sequelize.query(
				`SELECT 
                    count(*) as count, 
                    count(*) * product.price as price, 
                    product.name as name 
                FROM cart 
                    LEFT JOIN product ON product.id=cart.product_id
                WHERE cart.user_id=${user.id}
                GROUP BY cart.product_id
            `
			);
			res.send({ message: cart });
		} catch (err) {
			next(err);
		}
	}

	static async delete(req, res, next) {
		const { user } = req;
		try {
			await CartModel.destroy({ where: { userId: user.id } });
			res.send({ message: "deleted" });
		} catch (err) {
			next(err);
		}
	}
}

module.exports = CartService;
