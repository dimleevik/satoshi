const { Op } = require("sequelize");
const ProductModel = require("../../models/products/productModel");

class ProductService {
	static async create(req, res, next) {
		const product = req.body;
		try {
			await ProductModel.create(product);
			res.send({ message: "Created" });
		} catch (err) {
			next(err);
		}
	}

	static async list(req, res, next) {
		const { query } = req;
		try {
			const products = await ProductModel.findAndCountAll({
				where: {
					name: { [Op.like]: `%${query?.pattern || ""}%` },
				},
				attributes: ["id", "name", "price"],
			});
			res.send(products);
		} catch (err) {
			next(err);
		}
	}

	static async index(req, res, next) {
		const { params } = req;
		try {
			const product = await ProductModel.findOne({
				where: {
					id: params.id,
				},
			});
			res.send(product);
		} catch (err) {
			next(err);
		}
	}

	static async delete(req, res, next) {
		const { params } = req;
		try {
			await ProductModel.destroy({
				where: {
					id: params.id,
				},
			});
			res.send({ message: "deleted" });
		} catch (err) {
			next(err);
		}
	}

	static async update(req, res, next) {
		const { body, params } = req;
		try {
			const product = await ProductModel.update(body, {
				where: {
					id: params.id,
				},
			});
			res.send({ message: product });
		} catch (err) {
			next(err);
		}
	}
}

module.exports = ProductService;
