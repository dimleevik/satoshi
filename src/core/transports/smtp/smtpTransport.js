const nodemailer = require("nodemailer");
const smtpConfig = require("../../../common/config/smtpConfig");

module.exports = nodemailer.createTransport(smtpConfig);
