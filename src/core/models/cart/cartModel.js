const Sequelize = require("sequelize");
const connection = require("../db/dbConnection");
const ProductModel = require("../products/productModel");
const UserModel = require("../users/userModel");

const CartModel = connection.define(
	"Cart",
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		userId: {
			type: Sequelize.INTEGER,
			field: "user_id",
			references: {
				model: UserModel,
				key: "id",
			},
			allowNull: false,
		},

		productId: {
			type: Sequelize.INTEGER,
			field: "product_id",
			references: {
				model: ProductModel,
				key: "id",
			},
			allowNull: false,
		},
	},
	{
		tableName: "cart",
	}
);

module.exports = CartModel;
