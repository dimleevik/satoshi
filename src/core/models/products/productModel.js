const Sequelize = require("sequelize");
const connection = require("../db/dbConnection");

const ProductModel = connection.define(
	"Product",
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		name: {
			type: Sequelize.STRING,
			unique: false,
			allowNull: false,
		},
		description: {
			type: Sequelize.STRING,
			unique: false,
			allowNull: false,
		},
		price: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	},
	{
		tableName: "product",
	}
);

module.exports = ProductModel;
