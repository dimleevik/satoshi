const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const connection = require("../db/dbConnection");

const UserModel = connection.define(
	"User",
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		email: {
			type: Sequelize.STRING,
			unique: true,
		},
		password: {
			type: Sequelize.STRING,
		},
		role: {
			type: Sequelize.ENUM,
			values: ["admin", "user", "editor"],
		},
	},
	{
		tableName: "user",
	}
);

module.exports = UserModel;
