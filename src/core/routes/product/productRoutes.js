const { Router } = require("express");

const { body, query } = require("express-validator");
const { isAuthorized } = require("../../middlewares/auth/authMiddleware");
const validationMiddleware = require("../../middlewares/validation/validationMiddleware");
const ProductService = require("../../services/product/productService");

const router = Router();

router.post(
	"/",
	body("name").isString(),
	body("description").isString(),
	body("price").isInt(),
	validationMiddleware,
	isAuthorized(["admin", "editor"]),
	ProductService.create
);

router.get(
	"/",
	query("pattern").optional().isString(),
	validationMiddleware,
	isAuthorized(),
	ProductService.list
);

router.get("/:id", isAuthorized(), ProductService.index);

router.delete("/:id", isAuthorized(["admin"]), ProductService.delete);

router.put(
	"/:id",
	body("name").optional().isString(),
	body("description").optional().isString(),
	body("price").optional().isInt(),
	validationMiddleware,
	isAuthorized(["admin", "editor"]),
	ProductService.update
);

module.exports = router;
