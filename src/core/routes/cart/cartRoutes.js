const { Router } = require("express");

const { body, query } = require("express-validator");
const { isAuthorized } = require("../../middlewares/auth/authMiddleware");
const validationMiddleware = require("../../middlewares/validation/validationMiddleware");
const CartService = require("../../services/cart/cartService");
const ProductService = require("../../services/product/productService");

const router = Router();

router.post(
	"/",
	body("id").isInt(),
	validationMiddleware,
	isAuthorized(),
	CartService.create
);

router.get("/", validationMiddleware, isAuthorized(), CartService.list);

router.delete("/", isAuthorized(["admin"]), CartService.delete);

module.exports = router;
