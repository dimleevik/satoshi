const { Router } = require("express");
const AuthRoutes = require("./auth/authRoutes");
const ProductRoutes = require("./product/productRoutes");
const CartRoutes = require("./cart/cartRoutes");

const routes = Router();

routes.use("/auth", AuthRoutes);
routes.use("/product", ProductRoutes);
routes.use("/cart", CartRoutes);
module.exports = routes;
