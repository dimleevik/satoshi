const { Router } = require("express");

const AuthService = require("../../services/auth/authService");
const { body } = require("express-validator");
const validationMiddleware = require("../../middlewares/validation/validationMiddleware");

const router = Router();

router.post(
	"/login",
	body("email").isEmail(),
	body("password").isLength({ min: 6 }),
	validationMiddleware,
	AuthService.login
);

router.post(
	"/register",
	body("email").isEmail(),
	body("password").isLength({ min: 6 }),
	body("role").isIn(["admin", "user", "editor"]),
	validationMiddleware,
	AuthService.register
);

router.post(
	"/forgot",
	body("email").isEmail(),
	validationMiddleware,
	AuthService.forgot
);

module.exports = router;
