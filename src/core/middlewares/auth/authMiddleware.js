const JwtService = require("../../services/jwt/jwtService");

function isAuthorized(roles = []) {
	return (req, res, next) => {
		const [type, token] = req.headers.authorization.split(" ");
		if (type !== "Bearer") {
			req.status(400).send();
			return;
		}
		req.user = JwtService.verify(token);
		if (roles.find((role) => req.user.role === role) || !roles.length) {
			next();
			return;
		}
		res.status(400).send({ message: "Not authorized" });
	};
}

module.exports = { isAuthorized };
