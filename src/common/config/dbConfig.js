require("./initEnv");

module.exports = {
	dialect: "mysql",
	host: process.env.MYSQL_HOST || "localhost",
	port: process.env.MYSQL_PORT || 3306,
	username: process.env.MYSQL_USER,
	password: process.env.MYSQL_PASSWORD,
	database: process.env.MYSQL_DB,
	logging: false,
};
