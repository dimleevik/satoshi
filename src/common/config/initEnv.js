const dotenv = require("dotenv");

const path = process.env.NODE_ENV ? `.${process.env.NODE_ENV}.env` : ".env";

module.exports = dotenv.config({ path });
