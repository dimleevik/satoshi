FROM node:14-alpine

WORKDIR /app

COPY src ./src
COPY *.json ./
COPY .sequelizerc ./

RUN npm ci 

EXPOSE 3000
